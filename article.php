<?php include("path.php");?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

  <!-- Custom Styles -->
  <link rel="stylesheet" href="sets/css/styles.css">

  <title>Web Development blog</title>
</head>

<body>

 

  <!-- header -->
  <?php include(ROOT_PATH .'/app/includes/header.php');
  ?>
  <!-- // header -->

  
  <?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$articleID = $_GET['id'];
//DATABASE
$conn = mysqli_connect('localhost','mysql','mysql','blogfromscratch');


//Connection
if(!$conn){
    echo "Connection error:" . mysqli_connect_error();
}

//write query for the articles

$sql1= 'SELECT * FROM articles JOIN authors ON articles.author_id=authors.id where articles.id ='. $articleID . ' ';

//Query
$result1 = mysqli_query($conn,$sql1);

//Fetch
$datas = mysqli_fetch_array($result1);

$sql2 = ' SELECT category from categories inner join articles_categories on 
categories.id=articles_categories.category_id  where articles_categories.article_id= '. $articleID . ' ';

$result2 = mysqli_query($conn,$sql2);
$categories = "";  

    while($category = mysqli_fetch_array($result2)){
    $categories .= '<span>' .$category['category']. '</span>'; //je comprends pas ici
    // $categories = explode(" ", $categories);
    }











//  $datas = $reponse->fetch();
?>


  <div class="content clearfix">
      <div class="page-content">
        <!-- <h1 class="recent-posts-title">Recent Posts</h1> -->
        <div class="post clearfix">
          <img src="<?php echo $datas['image_url']; ?>" class="card-img" alt="..." style="max-width: 200px;">
          <div class="post-content">

            <h2 class="post-title"><a href="#"><?= $datas['title'];?> </a></h2>

            <div class="post-info">
              <i class="fa fa-user-o"></i> Author: <?= $datas['firstname']. ' ' . $datas['lastname']; ?>
              &nbsp;
              <i class="fa fa-calendar"></i> <?= $datas['published_at'];?>
              &nbsp;
              <i class="fa fa-calendar"> </i>Reading_time: <?= $datas['reading_time'];?>
              &nbsp;
              <i class="fa fa-calendar"> </i> Category: <?= $categories;?>

             
            </div>
            <p class="post-body"><?= $datas['content']; ?>.
            </p>
         
        
            
          </div>
        </div>
      
      </div>
</div>


  
  <!-- // page wrapper -->

  <!-- FOOTER -->
 
  <!-- // FOOTER -->


  <!-- JQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Slick JS -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

 
  <script src="sets/js/script.js"></script>

</body>

</html>