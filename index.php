<?php include("path.php");?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

  <!-- Custom Styles -->
  <link rel="stylesheet" href="sets/css/styles.css">

  <title>Blog From Scratch</title>
</head>

<body>

  <!-- header -->
  <?php include(ROOT_PATH .'/app/includes/header.php');


?>
  <!-- // header -->


  <div class="page-wrapper">

    
    <h1 class="recent-posts-title">Recent Posts</h1>
    <?php

    // database connection
     $conn = mysqli_connect('localhost','HAZELLE','HAZELLE','blogfromscratch');


     //connection status
     if(!$conn){
         echo "Connection error:" . mysqli_connect_error();
     }
     
     
     //requette
     $sql = 'SELECT * FROM articles ORDER BY published_at DESC ';
     $result = mysqli_query($conn,$sql);
     
          
      while($datas = mysqli_fetch_array($result)){ 
     
                 // Category
                 $sql2 = ' SELECT category from categories inner join articles_categories on 
                 categories.id=articles_categories.category_id  where articles_categories.article_id= '. $datas['id'] . ' ';
                 
                 $result2 = mysqli_query($conn,$sql2);
                 $categories = "";   
                 
                     while($category = mysqli_fetch_array($result2)){
                     $categories .= '<span>' .$category['category']. '</span>'; 
                   
                     }
     
     
                   //Author  
                  $sql3 = ' SELECT firstname,lastname from authors inner join articles on 
                  authors.id=articles.author_id where authors.id= '. $datas['id'] . ' ';
                  
                  $result3 = mysqli_query($conn,$sql3);
     
                  while($author = mysqli_fetch_array($result3)){
                      $firstname = $author['firstname'];
                      $lastname = $author['lastname'];
                     }
     
                 
                 ?>

<div class="content clearfix">
      <div class="page-content">
       
        <div class="post clearfix">
          <img src="<?php echo $datas['image_url']; ?>" class="card-img" alt="..." style="max-width: 200px;">
          <div class="post-content">

            <h2 class="post-title"><a href="#"><?= $datas['title'];?> </a></h2>

            <div class="post-info">
              <i class="fa fa-user-o"></i> Author: <?= $firstname. ' ' . $lastname; ?>
              &nbsp;
              <i class="fa fa-calendar"></i> <?= $datas['published_at'];?>
              &nbsp;
              <i class="fa fa-calendar"> </i>Reading_time:<?= $datas['reading_time'];?>
              &nbsp;
              <i class="fa fa-calendar"> </i> Category: <?= $categories;?>
             
             
            </div>
            <p class="post-body"><?= substr($datas['content'], 0, 300); ?>.
            </p>
         
            <a href="article.php?id=<?= $datas['id']; ?>" class="read-more">Lire La Suite</a>
            
          </div>
        </div>
      
      </div>
</div>


<?php
}


?>


    <!-- content -->
   
<div class="content clearfix">
      <div class="sidebar">
        <!-- Search -->
        <div class="search-div">
          <form action="index.php" method="post">
            <input type="text" name="search-term" class="text-input" placeholder="Search...">
          </form>
        </div>
        <!-- // Search -->

        <!-- topics -->
        <div class="section topics">
          <h2>Categories</h2>
          <ul>
            <a href="#">
              <li>API</li>
            </a>
            <a href="#">
              <li>Performance</li>
            </a>
            <a href="#">
              <li>IDE</li>
            </a>
            <a href="#">
              <li>VS CODE</li>
            </a>
            
          </ul>
        </div>
        <!-- // topics -->

      </div>
    </div>
    <!-- // content -->

  </div>
  <!-- // page wrapper -->
 

  <!-- FOOTER -->
  <?php include('app/includes/footer.php');


?>
  <!-- // FOOTER -->


  <!-- JQuery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Slick JS -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

  <script src="sets/js/script.js"></script>

</body>

</html>