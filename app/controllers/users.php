<?php

include("app/database/db.php");
$author_id = create('authors', $_POST);
$author = selectOne('authors', ['id' => $author_id]);

$_SESSION['id'] = $author['id'];
$_SESSION['firstname'] = $author['firstname'];
$_SESSION['message'] = "You are now logged in";
$_SESSION['type'] = 'success';
header('location: ../../index.php');
exit();

?>